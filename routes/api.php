<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
*/

Route::group(['namespace' => 'Api\Catalog', 'as' => 'api.catalog.'], function()
{
    Route::group(['namespace' => '\Companies'], function()
    {
        Route::resource('companies', 'CompanyController', ['only' => [
            'show'
        ]]);
    });
    Route::group(['namespace' => '\Buildings'], function()
    {
        Route::resource('buildings', 'BuildingController', ['only' => [
            'index', 'show'
        ]]);
    });
    Route::group(['namespace' => '\Categories'], function()
    {
        Route::resource('categories', 'CategoryController', ['only' => [
            'show'
        ]]);
    });
    Route::group(['prefix' => 'search', 'as' => 'search.', 'namespace' => '\Search'], function()
    {
        Route::get('/around', ['as' => 'around', 'uses' => 'SearchController@companiesAround']);
        Route::get('/polygon', ['as' => 'polygon', 'uses' => 'SearchController@companiesInPolygon']);
        Route::get('/name/{name?}', ['as' => 'name', 'uses' => 'SearchController@byName']);
        Route::get('/categories/{category}', ['as' => 'categories', 'uses' => 'SearchController@byCategories']);

    });
});

