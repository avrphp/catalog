<?php

namespace App\Http\Controllers\Api\Catalog\Categories;

use Illuminate\Routing\Controller as BaseController;
use App;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Validator;
use Log;
use Illuminate\Http\Request;

class CategoryController extends BaseController
{
    /**
     * Выводим информацию об организации по рубрике
     *
     * @param Request $request
     * @return mixed
     */
    public function show(Request $request)
    {
        try {
            $category = App\Models\Category::findOrFail($request->category);
            return response()->json([
                'meta' => ['code' => 200],
                'result' =>  [
                    'category' => $category,
                    'companies' => $category->companies()->paginate()
                ]
            ]);
        } catch(Exception $e) {
            if ($e instanceof ModelNotFoundException) {
                return response()->json([
                    'meta' => ['code' => 400]
                ]);
            }
            Log::error($e->getMessage() . PHP_EOL);
            return response()->json([
                'meta' => ['code' => 500]
            ]);
        }
    }
}
