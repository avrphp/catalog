<?php

namespace App\Http\Controllers\Api\Catalog\Search;

use Illuminate\Routing\Controller as BaseController;
use App;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Validator;
use Log;
use Illuminate\Http\Request;

class SearchController extends BaseController
{
    /**
     * Ищем организации которые находятся
     * в заданной области (радиусе)
     *
     * @param Request $request
     * @return mixed
     */
    public function companiesAround(Request $request)
    {
        try {
            $data = $request->all();
            $validator = Validator::make($data, [
                'lat'       => 'required|numeric',
                'lng'       => 'required|numeric',
                'radius'    => 'required|numeric',
            ]);
            if ($validator->fails()) {
                return response()->json([
                    'meta' => ['code' => 400]
                ]);
            }
            $results = App\Models\Building::search('', function ($algolia, $query, $options) use ($data) {
                $location =  [
                    'aroundLatLng' => $data['lat'] . ',' . $data['lng'],
                    'aroundRadius' => $data['radius']
                ];
                $options = array_merge($options, $location);
                return $algolia->search($query, $options);
            })->paginate();
            $results->load('companies');
            return response()->json([
                'meta' => ['code' => 200],
                'result' => $results
            ]);
        } catch(Exception $e) {
            if ($e instanceof ModelNotFoundException) {
                return response()->json([
                    'meta' => ['code' => 400]
                ]);
            }
            Log::error($e->getMessage() . PHP_EOL);
            return response()->json([
                'meta' => ['code' => 500]
            ]);
        }
    }

    /**
     * Ищем организации которые находятся
     * в заданной области (полигоне)
     *
     * @param Request $request
     * @return mixed
     */
    public function companiesInPolygon(Request $request)
    {
        try {
            $data = $request->all();
            $validator = Validator::make($data, [
                'lat1'  => 'required|numeric',
                'lng1'  => 'required|numeric',
                'lat2'  => 'required|numeric',
                'lng2'  => 'required|numeric',
                'lat3'  => 'required|numeric',
                'lng3'  => 'required|numeric',
                'lat4'  => 'required|numeric',
                'lng4'  => 'required|numeric',
            ]);
            if ($validator->fails()) {
                return response()->json([
                    'meta' => ['code' => 400]
                ]);
            }
            $results = App\Models\Building::search('', function ($algolia, $query, $options) use ($data) {
                $polygonAroundTheUK = [
                    (float) $data['lat1'], (float) $data['lng1'], // Точка 1
                    (float) $data['lat2'], (float) $data['lng2'], // Точка 2
                    (float) $data['lat3'], (float) $data['lng3'], // Точка 3
                    (float) $data['lat4'], (float) $data['lng4'], // Точка 4
                ];
                $location = [
                    'insidePolygon' => [$polygonAroundTheUK]
                ];
                $options = array_merge($options, $location);
                return $algolia->search($query, $options);
            })->paginate();
            $results->load('companies');
            return response()->json([
                'meta' => ['code' => 200],
                'result' => $results
            ]);
        } catch(Exception $e) {
            if ($e instanceof ModelNotFoundException) {
                return response()->json([
                    'meta' => ['code' => 400]
                ]);
            }
            Log::error($e->getMessage() . PHP_EOL);
            return response()->json([
                'meta' => ['code' => 500]
            ]);
        }
    }

    /**
     * Поиск организаций по имени
     *
     * @param Request $request
     * @return mixed
     */
    public function byName(Request $request)
    {
        try {
            $results = App\Models\Company::search($request->name)->paginate();
            $results->load('building');
            return response()->json([
                'meta' => ['code' => 200],
                'result' =>  $results
            ]);
        } catch(Exception $e) {
            if ($e instanceof ModelNotFoundException) {
                return response()->json([
                    'meta' => ['code' => 400]
                ]);
            }
            Log::error($e->getMessage() . PHP_EOL);
            return response()->json([
                'meta' => ['code' => 500]
            ]);
        }
    }

    /**
     * Поиск организаци по ветке катекории
     *
     * @param Request $request
     * @return mixed
     */
    public function byCategories(Request $request)
    {
        try {
            $category = App\Models\Category::findOrFail($request->category);
            $descendants = $category->descendants()->pluck('id')->toArray();
            $companies = App\Models\Company::with('categories')->whereHas('categories', function($query) use ($category, $descendants) {
                $query->whereIn('id', array_merge([$category->id], $descendants));
            })->paginate();
            return response()->json([
                'meta' => ['code' => 200],
                'result' => $companies

            ]);
        } catch(Exception $e) {
            if ($e instanceof ModelNotFoundException) {
                return response()->json([
                    'meta' => ['code' => 400]
                ]);
            }
            Log::error($e->getMessage() . PHP_EOL);
            return response()->json([
                'meta' => ['code' => 500]
            ]);
        }
    }
}
