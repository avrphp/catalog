<?php

namespace App\Http\Controllers\Api\Catalog\Buildings;

use Illuminate\Routing\Controller as BaseController;
use App;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Validator;
use Log;
use Illuminate\Http\Request;

class BuildingController extends BaseController
{
    /**
     * Выводим список всех зданий
     *
     * @param Request $request
     * @return mixed
     */
    public function index()
    {
        try {
            return response()->json([
                'meta' => ['code' => 200],
                'result' => ['items' =>  App\Models\Building::paginate()]
            ]);
        } catch(Exception $e) {
            Log::error($e->getMessage() . PHP_EOL);
            return response()->json([
                'meta' => ['code' => 500]
            ]);
        }
    }

    /**
     * Выводим информацию о здании
     *
     * @param Request $request
     * @return mixed
     */
    public function show(Request $request)
    {
        try {
            $building = App\Models\Building::findOrFail($request->building);
            return response()->json([
                'meta' => ['code' => 200],
                'result' =>  [
                    'building' => $building,
                    'companies' => $building->companies()->with([
                        'categories' => function($query) {
                            $query->select(['id', 'name']);
                        }
                    ])->get(),
                    'total' => $building->companies()->count()
                ]
            ]);
        } catch(Exception $e) {
            if ($e instanceof ModelNotFoundException) {
                return response()->json([
                    'meta' => ['code' => 400]
                ]);
            }
            Log::error($e->getMessage() . PHP_EOL);
            return response()->json([
                'meta' => ['code' => 500]
            ]);
        }
    }
}
