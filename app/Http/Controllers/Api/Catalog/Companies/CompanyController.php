<?php

namespace App\Http\Controllers\Api\Catalog\Companies;

use Illuminate\Routing\Controller as BaseController;
use App;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Validator;
use Log;
use Illuminate\Http\Request;

class CompanyController extends BaseController
{
    /**
     * Выводим информацию о компании
     *
     * @param Request $request
     * @return mixed
     */
    public function show(Request $request)
    {
        try {
            $company = App\Models\Company::findOrFail($request->company);
            return response()->json([
                'meta' => ['code' => 200],
                'result' =>  [
                    [
                        'data' => $company,
                        'categories' => $company->categories()->get()
                    ]
                ]
            ]);
        } catch(Exception $e) {
            if ($e instanceof ModelNotFoundException) {
                return response()->json([
                    'meta' => ['code' => 400]
                ]);
            }
            Log::error($e->getMessage() . PHP_EOL);
            return response()->json([
                'meta' => ['code' => 500]
            ]);
        }
    }
}
