<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class Company extends Model
{
    use Searchable;

    /**
     * Скрываем необходимые аттрибуты для серриализации
     *
     * @var array
     */
    protected $hidden = ['pivot', 'created_at', 'updated_at', 'building_id'];

    /**
     * Получаем здание компании
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function building()
    {
        return $this->belongsTo(Building::class);
    }

    /**
     * Получаем категории компании
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    /**
     * Получаем декодированный из json список контактов
     *
     * @param  string $value
     * @return mixed
     */
    public function getContactsAttribute($value)
    {
        return json_decode($value);
    }

    /**
     * Преобразуем массив контактов $value в json представление
     *
     * @param  array $value
     * @return void
     */
    public function setContactsAttribute($value)
    {
        $this->attributes['contacts'] = json_encode($value);
    }

    /**
     * Получаем индексируемый массив для модели
     *
     * @return array
     */
    public function toSearchableArray()
    {
        $record = $this->toArray();
        $record['building'] = $this->building->toArray();
        return $record;
    }

    /**
     * Получаем имя индекса для модели
     *
     * @return string
     */
    public function searchableAs()
    {
        return 'companies_name_index';
    }

    public function getAlgoliaRecord()
    {

        $this->building;
        return $this;

    }
}
