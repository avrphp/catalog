<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class Building extends Model
{
    use Searchable;

    /**
     * Скрываем необходимые аттрибуты для серриализации
     *
     * @var array
     */
    protected $hidden = ['created_at', 'updated_at'];

    /**
     * Получаем все кампании находящиеся в здании
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function companies()
    {
        return $this->hasMany(Company::class);
    }

    /**
     * Получаем индексируемый массив для модели
     *
     * @return array
     */
    public function toSearchableArray()
    {
        $record = $this->toArray();
        $record['companies'] = $this->companies->toArray();
        $record['_geoloc'] = [
            'lat' => $record['lat'],
            'lng' => $record['lng'],
        ];

        unset($record['lat'], $record['lng']);
        return $record;
    }

    /**
     * Получаем имя индекса для модели
     *
     * @return string
     */
    public function searchableAs()
    {
        return 'buildings_lat_lng_index';
    }
}
