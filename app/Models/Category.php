<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Kalnoy\Nestedset\NodeTrait;

class Category extends Model
{
    use NodeTrait;

    /**
     * Скрываем необходимые аттрибуты для серриализации
     *
     * @var array
     */
    protected $hidden = ['pivot', '_lft', '_rgt', 'parent_id'];


    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Получаем все кампании в категории
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function companies()
    {
        return $this->belongsToMany(Company::class);
    }
}
