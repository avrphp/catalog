<?php

use Illuminate\Database\Seeder;
use App\Models\Category;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->truncate();
        Category::create([
            'name' => 'Еда',
            'children' => [
                ['name' => 'Полуфабрикаты оптом'],
                ['name' => 'Мясная продукция']
            ]
        ]);

        Category::create([
            'name' => 'Автомобили',
            'children' => [
                ['name' => 'Грузовые'],
                ['name' => 'Легковые',
                    'children' => [
                        ['name' => 'Запчасти для подвески'],
                        ['name' => 'Шины/Диски']
                    ]
                ]
            ]
        ]);
        Category::create([
            'name' => 'Спорт',
            'children' => [
                ['name' => 'Волоспорт',
                    'children' => [
                        ['name' => 'Велосипеды'],
                        ['name' => 'Защита']
                    ]
                ],
                ['name' => 'Бег',
                    'children' => [
                        ['name' => 'Мужская одежда',
                            'children' => [
                                ['name' => 'Брюки',
                                    'children' => [
                                        ['name' => 'Спортивные костюмы']
                                    ]],
                                ['name' => 'Майки']
                            ]
                        ],
                        ['name' => 'Женская одежда']
                    ]
                ]
            ]
        ]);
        Category::create([
            'name' => 'Отдых'
        ]);
    }
}
