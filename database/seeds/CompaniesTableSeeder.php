<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class CompaniesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('companies')->truncate();
        $faker = Faker::create('ru_RU');
        foreach (range(1,1000) as $index) {
            $category = App\Models\Category::inRandomOrder()->first();
            $descendants = array_slice($category->descendants()->pluck('id')->toArray(), 0, random_int(1, 4));
            $company = App\Models\Company::create([
                'name' => $faker->company,
                'contacts' => array_slice([
                    $faker->phoneNumber,
                    $faker->phoneNumber,
                    $faker->phoneNumber,
                    $faker->phoneNumber,
                    $faker->phoneNumber,
                ], 0, random_int(1, 5)),
                'building_id' => App\Models\Building::inRandomOrder()->first()->id,
                'created_at' => $faker->dateTime($max = 'now'),
                'updated_at' => $faker->dateTime($max = 'now'),
            ]);
            $company->categories()->sync(array_merge([$category->id], $descendants));
        }
    }
}
