<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class BuildingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('buildings')->truncate();
        $faker = Faker::create('ru_RU');
        foreach (range(1,1000) as $index) {
            DB::table('buildings')->insert([
                'address' => $faker->streetAddress,
                'lat' => $faker->latitude($min = -90, $max = 90),
                'lng' => $faker->longitude($min = -180, $max = 180) ,
                'created_at' => $faker->dateTime($max = 'now'),
                'updated_at' => $faker->dateTime($max = 'now'),
            ]);
        }
    }
}
