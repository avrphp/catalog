
# Справочник 


Метод    |            URI                   |          Имя                  |                              Описание                                  |                  Параметры                   |                     Ответ                      | Коды ошибок
---------|----------------------------------|-------------------------------|------------------------------------------------------------------------|----------------------------------------------|------------------------------------------------|------------
GET HEAD | api/buildings                    | api.catalog.buildings.index   | список всех зданий                                                     | page                                         | id,address,lat,lng                             | 200,400,500*
GET HEAD | api/buildings/{building}         | api.catalog.buildings.show    | выдача всех организаций находящихся в конкретном здании                | building                                     | id,address,lat,lng,companies                   | 200,400,500*
GET HEAD | api/categories/{category}        | api.catalog.categories.show   | список организаций находящихся в категории                             | category,page                                | category{id,name}, companies{id,name,contacts} | 200,400,500*
GET HEAD | api/companies/{company}          | api.catalog.companies.show    | выдачаинформации об организациях по их идентификаторам                 | company                                      | id,name,contacts,categories{id,name}           | 200,400,500*
GET HEAD | api/search/around                | api.catalog.search.around     | список организаций, которые находятся в заданном радиусе               | lat,lng,radius(в метрах),page                | id,address,lat,lng,companies{id,name,contacts} | 200,400,500*
GET HEAD | api/search/categories/{category} | api.catalog.search.categories | список организаций, находящихся в ветке категории                      | category,page                                | id,name,contacts,categories{id,name}           | 200,400,500*
GET HEAD | api/search/name/{name?}          | api.catalog.search.name       | поиск организации по названию                                          | name,page                                    | id,name,contacts,building{id,address,lat,lng}  | 200,400,500*
GET HEAD | api/search/polygon               | api.catalog.search.polygon    | список организаций, которые находятся в заданной прямоугольной области | lat1,lng1,lat2,lng2,lat3,lng3,lat4,lng4,page | id,address,lat,lng,companies{id,name,contacts} | 200,400,500*
                                                                              
* 200 - OK, 400 - неверный формат данных, 500 - ошибка сервера.